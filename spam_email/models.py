from django.db import models
from django.urls import reverse


# Create your models here.
class Email(models.Model):
    content = models.TextField()
    result = models.CharField(max_length=100)
    accuracy = models.CharField(max_length=100)

    def __str__(self):
        return self.content

    def get_absolute_url(self):
        return reverse('history', args=[str(self.id)])
