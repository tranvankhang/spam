import pickle
import string
from nltk.corpus import stopwords


def text_process(mess):
    nopunc = [char for char in mess if char not in string.punctuation]
    nopunc = ''.join(nopunc)
    clean_mess = [word for word in nopunc.split() if word.lower() not in stopwords.words('english')]
    return clean_mess


def importPipelines():
    pipeline = pickle.load(open('text_clf_pipeline.pkl', 'rb'))
    pipeline_second = pickle.load(open('textClf.pkl', 'rb'))
    return pipeline, pipeline_second
